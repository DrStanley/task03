﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task03.Services
{
	class SwitchGrade
	{
		public static string GetRemark(char c)
		{
			string remark="";
			switch (c) {
				case 'A':
					remark = "Exellent";
					break;
				case 'B':
					remark = "Very Good";
					break;
				case 'C':
					remark = "Good";
					break;
				case 'D':
					remark = "Poor";
					break;
				case 'F':
					remark = "Very Poor";
					break;
				default:
					remark = "Very Poor";
					break;
			}


			return remark;
		}
	}
}
