﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task03.Services
{
	class WhileAndDowhile
	{
		public static void WhileLoop()
		{
			Console.WriteLine("Enter a number:");
			int val = Convert.ToInt32(Console.ReadLine());
			int count =1;
			string EvenOutput = "";
			string OddOutput = "";
			while (count < val)
			{
				if (NumberCheck.isEven(count))
				{
					EvenOutput += count.ToString()+",";
				}
				else
				{
					OddOutput += count.ToString() + ",";

				}
				count++;
			}
			Console.WriteLine($"Even numbers in {val}: {EvenOutput}");
			Console.WriteLine($"Odd numbers in {val}: {OddOutput}");
			
		}
		public static void DoWhileLoop()
		{

			int num;
			do
			{
				System.Random rnd = new System.Random();
				num= rnd.Next(1,200);

				Console.WriteLine(num);

			} while (num<200);


		}
	}
}
