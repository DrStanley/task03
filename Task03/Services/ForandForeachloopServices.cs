﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task03.Services
{
	class ForandForeachloopServices
	{
		static List<string> grades = new List<string>();
		static List<string> students = new List<string>();
		static List<string> course = new List<string>();
		public static void GetGrades()
		{
			for(int i = 0; i <= 5; i++)
			{
				grades.Clear();
				System.Random rnd = new System.Random();
				int score = rnd.Next(10,100);
				char grade = IfelseIf.GetGrade(score);
				grades.Add(grade.ToString());
			}

		}

		public static void AddNamesAndCourse()
		{
			//namespace of students
			students.Add("James"); 
			students.Add("Chinedu"); 
			students.Add("Joy");
			students.Add("Chisom"); 
			
			//courses offered
			course.Add("GST101");
		
		
		}

		public static void DisplayCourse()
		{
			AddNamesAndCourse();
			foreach(var sub in course){

				Console.Write("\t" + sub);
			}


		}
		public static void DisplayResult()
		{
			DisplayCourse();
			for(int i = 0; i < students.Count; i++)
			{
				Console.Write("\n"+students.ElementAt(i));
				GetGrades();
				foreach (var grad in grades)
				{
					Console.Write("\t  " + grad);
				}

			}


		}
	}
}
