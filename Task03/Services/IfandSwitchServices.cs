﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task03.Services
{
	class IfandSwitchServices
	{
		public static string IfStatement(){
			Console.WriteLine("Enter a number:");
			int val = Convert.ToInt32(Console.ReadLine());
			bool ans = NumberCheck.isEven(val);
			string output;
			if (ans)
			{
				output= "You Entered an even Number";
			}
			else
			{
				output = "You Entered an odd Number";
			}
			return output;
		}	
		public static char IfelseIfStatement(){
			Console.WriteLine("Enter score:");
			int val = Convert.ToInt32(Console.ReadLine());
			char grade = IfelseIf.GetGrade(val);
			return grade;
		}

		public static string SwitchStatement(char a)
		{
			string output = "Remarks: ";
			output += SwitchGrade.GetRemark(a);
			return output;
		}


	}
}
