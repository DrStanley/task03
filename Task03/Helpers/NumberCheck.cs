﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task03.Services
{
	class NumberCheck
	{
		public static bool isOdd(int x)
		{
			bool ans;
			if (x % 2 != 0)
			{

				ans = true;
			}
			else
			{
				ans = false;
			}

			return ans;
		}

		public static bool isEven(int x)
		{
			bool ans;
			if (x % 2 == 0)
			{

				ans = true;
			}
			else
			{
				ans = false;
			}

			return ans;
		}
	}
}
