﻿using System;
using Task03.Services;

namespace Task03
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("If stetement to check if a number is even or odd");
			Console.WriteLine(IfandSwitchServices.IfStatement());
			Console.WriteLine("......");
			Console.WriteLine("If else if stetement to check grade of a score...");
			char a = IfandSwitchServices.IfelseIfStatement();
			Console.WriteLine("Grade: "+a);
			Console.WriteLine("......");
			Console.WriteLine("Switch stetement to get Remarks of grade...");
			Console.WriteLine(IfandSwitchServices.SwitchStatement(a));
			Console.WriteLine("......");
			Console.WriteLine("For and ForEach loop to compute student Grade...");
			DispalyLoopResult.DisplayStudentResult();
			Console.WriteLine("......");
			Console.WriteLine("While loop to compute even and odd numbers from an input...");
			WhileAndDowhile.WhileLoop();
			Console.WriteLine("......");
			Console.WriteLine("DoWhile loop to print random numbers, to stop if the numer>200...");

			WhileAndDowhile.DoWhileLoop();

		}
	}
}
